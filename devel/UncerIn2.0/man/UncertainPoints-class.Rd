\name{UncertainPoints-class}
\Rdversion{1.1}
\docType{class}
\alias{UncertainPoints-class}

\title{Class \code{"UncertainPoints"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Objects from the Class}{
Objects can be created by calls of the form \code{new("UncertainPoints", ...)}.
%%  ~~ describe objects here ~~ 
}
\section{Slots}{
  \describe{
    \item{\code{x}:}{Object of class \code{"numeric"} ~~ }
    \item{\code{y}:}{Object of class \code{"numeric"} ~~ }
    \item{\code{uncertaintyLower}:}{Object of class \code{"numeric"} ~~ }
    \item{\code{modalValue}:}{Object of class \code{"numeric"} ~~ }
    \item{\code{uncertaintyUpper}:}{Object of class \code{"numeric"} ~~ }
  }
}
\section{Methods}{
No methods defined with class "UncertainPoints" in the signature.
}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("UncertainPoints")
}
\keyword{classes}
